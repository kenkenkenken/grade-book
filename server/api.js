// Express router for our API.
// Every URL starting with /api/ will be directed here
// This is a basic CRUD API for our Users MongoDB database

const express = require('express');
const router = express.Router();  // get an instance of the express Router
const mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/grade-book');

//Define our schema for Grade Book
const QuarterGradeSchema = mongoose.Schema({
  quarterName: String,
  average: Number
});

const StudentSchema = mongoose.Schema({
  name: String,
  grades: [QuarterGradeSchema],
  finalGrade: Number
});

const Student = mongoose.model("Student", StudentSchema);

router.get('/get-grades', (req, res) => {
  Student.find((err, students) => {
    if(err) {
      console.log(err);
      res.send([]);
    } else {
      students.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      res.json(students);
    }
  });
});

router.delete('/clear-all-grades', (req, res) => {
  deleteAllGrades().then(r => {});
  res.send("success");
});

router.post('/upsert-grades', (req, res) => {
  const lines = req.body.gradeInput.split('\n');
  let currentQuarter = '';
  let students = new Map();
  let quarterFormat = /[a-zA-Z]+ [0-9]+, [0-9]+/i;
  for (let i = 0; i < lines.length; i++) {
    if (quarterFormat.test(lines[i])) {
      currentQuarter = lines[i];
    } else {
      const currentLine = lines[i];
      const currentLineItems = currentLine.split(" ");
      let addToHomework = false, addToTest = false, name = [], tests = [], homeworks = [];
      for (let j = 0; j < currentLineItems.length; j++) {
        if (!isNumeric(currentLineItems[j])) {
          if (currentLineItems[j] === "H") {
            addToHomework = true;
            addToTest = false;
          } else if (currentLineItems[j] === "T") {
            addToTest = true;
            addToHomework = false;
          } else {
            name.push(currentLineItems[j]);
          }
        } else {
          if (addToHomework) {
            homeworks.push(Number(currentLineItems[j]));
          } else if (addToTest) {
            tests.push(Number(currentLineItems[j]));
          }
        }
      }
      homeworks = removeSmallestValue(homeworks);
      const homeworkTotal = homeworks.reduce((a, b) => a + b, 0);
      const homeworkAverage = (homeworkTotal / homeworks.length) || 0;
      const testTotal = tests.reduce((a, b) => a + b, 0);
      const testAverage = (testTotal / tests.length) || 0;
      const studentGrade = {
        quarterName: currentQuarter,
        average: toDecimalRoundedOff(((homeworkAverage * 0.4) + (testAverage * 0.6)), 1)
      }
      if (!students.has(name.join(" "))) {
        let currentArrayGrades = [];
        currentArrayGrades.push(studentGrade)
        students.set(name.join(" "), currentArrayGrades);
      } else {
        let currentArrayGrades = students.get(name.join(" "));
        currentArrayGrades.push(studentGrade);
        students.set(name.join(" "), currentArrayGrades);
      }
    }
  }
  students.forEach(processStudentGrades);
  res.send("success");
});

function isNumeric(str) {
  if (typeof str != "string") return false;
  return !isNaN(str) && !isNaN(parseFloat(str));
}

function removeSmallestValue(arr) {
  const smallest = Math.min(...arr);
  const index = arr.indexOf(smallest);
  return arr.filter((_, i) => i !== index);
}

function toDecimalRoundedOff(amount, toFixed) {
  return parseFloat((Math.round(amount * 100) / 100).toFixed(toFixed))
}

async function deleteAllGrades() {
  await Student.deleteMany({});
}

async function processStudentGrades(value, key) {
  const finalGradeTotal = value.reduce((a, b) => a + b.average, 0);
  const finalGrade = (finalGradeTotal / value.length) || 0;
  const student = new Student({
    name: key,
    grades: value,
    finalGrade: toDecimalRoundedOff(finalGrade, 1)
  });
  const filter = {name: key};
  const existingRecord = await Student.findOne(filter);
  if (existingRecord && existingRecord.id !== null) {
    student._id = existingRecord.id;
    await Student.update(filter, student);
  } else {
    await student.save();
  }
}

module.exports = router

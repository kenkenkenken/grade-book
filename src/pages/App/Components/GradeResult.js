import React, {Component} from 'react';
import '../style.css'

export default class GradeResult extends Component {

	render() {
		return (
			<div>
				<h5>Results: </h5>
				{ !this.props.gradeIsLoading ? this.props.displayResults.map((result, index) => (
					<div key={index}>
						<h5> {result.quarterName} </h5>
						{ result.studentGrades.map((details, index) => (
							<div key={index}>
								<p> {details.studentName}: {details.averageGrade} </p>
							</div>
						))}
						<br/>
					</div>
				)) : <div className="loader"/> }
				{ this.props.displayResults && this.props.displayResults.length > 0 ?
					<button type="button" onClick={this.props.clearAllGrades}>Clear ALL Grades</button> : null }
			</div>
		);
	}
}

import React, { Component } from 'react';
import '../style.css'
import GradeInput from "./GradeInput";
import GradeResult from "./GradeResult";

export default class GradeContainer extends Component {
	constructor(props) {
		super(props);
		this.state = { displayResults: [], gradeInput: "", gradeIsLoading: true };
	}

	componentDidMount() {
		this.getGrades();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.gradeIsLoading !== this.state.gradeIsLoading) {
			this.getGrades();
		}
	}

	getGrades = () => {
		fetch("api/get-grades")
			.then((res) =>  {
				return res.json();
			}).then((json) => {
				let quarters = [];
				for (const [key, value] of json.entries()) {
					let quartersPerStudent = value.grades.map(function(elem){
						return elem.quarterName;
					});
					quarters = [...new Set(quarters.concat(quartersPerStudent))];
				}
				if (quarters.length >= 1) {
					quarters.push("Final Average")
				}
				let displayResultsArray = [];
				quarters.forEach(quarterName => {
					let displayResult = {
						quarterName: quarterName
					}
					let studentGrades = [];
					for (const [key, value] of json.entries()) {
						value.grades.map(function(elem){
							if (elem.quarterName === quarterName) {
								let studentGrade = {
									studentName: value.name,
									averageGrade: elem.average
								}
								studentGrades.push(studentGrade);
							}
						});
						if (quarterName === "Final Average") {
							let studentGrade = {
								studentName: value.name,
								averageGrade: value.finalGrade
							}
							studentGrades.push(studentGrade);
						}
					}
					displayResult.studentGrades = studentGrades;
					displayResultsArray.push(displayResult);
				});
				this.setState({ displayResults: displayResultsArray, gradeIsLoading: false })
			});
	}

	upsertGrades = () => {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ gradeInput: this.state.gradeInput })
		};
		fetch('api/upsert-grades', requestOptions)
			.then((res) =>  {
				this.setState({ gradeIsLoading: true })
			}).then((json) => {

		});
	}

	clearAllGrades = () => {
		const requestOptions = {
			method: 'DELETE',
			headers: { 'Content-Type': 'application/json' }
		};
		fetch('api/clear-all-grades', requestOptions)
			.then((res) =>  {
				this.setState({ gradeIsLoading: true })
			}).then((json) => {
		});
	}

	handleTextAreaChange = (event) => {
		this.setState({ gradeInput: event.target.value });
	}

	render() {
		return (
			<div className="row">
				<div className="column">
					<GradeInput gradeInput={this.state.gradeInput}
						upsertGrades={this.upsertGrades}
						handleTextAreaChange={this.handleTextAreaChange}/>
				</div>
				<div className="column">
					<GradeResult displayResults={this.state.displayResults}
				 		gradeIsLoading={this.state.gradeIsLoading}
				 		clearAllGrades={this.clearAllGrades}/>
				</div>
			</div>
		);
	}
}

import React, {Component} from 'react';
import '../style.css'

export default class GradeInput extends Component {
	render() {
		return (
			<div>
				<h5>Input Grades: </h5>
				<textarea name="gradesTextArea"
						  placeholder="Paste Student Grades Here"
						  onChange={this.props.handleTextAreaChange} />
				<button type="button" onClick={this.props.upsertGrades}>
					Calculate Grades
				</button>
			</div>
		);
	}
}

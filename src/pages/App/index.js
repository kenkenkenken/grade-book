import React, { Component } from 'react';
import './style.css';
import GradeContainer from "./Components/GradeContainer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Grade Book</h2>
        </div>
        <GradeContainer />
      </div>
    );
  }
}

export default App;

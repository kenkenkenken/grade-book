# grade-book
A simple MERN stack application that records homework and test grades of students per quarter per year.

## Pre-requisites

- Node version >= v14.19
- Mongo Community Server

## Running

To run the project, I recommend having the following packages installed:
- npm install nodemon (used for running the development environment)
- npm install pm2 -g (used for running the production environment)

Run DEV environment via 'npm run dev' command. This runs the react app on port 3000, and the express api and other backend stuff on port 9000. Access the site through http://localhost:3000. API calls are proxied to the server on port 9000. Whenever you make a change to any file, the server will be restarted automatically. Just refresh to see your changes in action.

## MongoDB Schema

QuarterGradeSchema (Schema that holds the average grade for a specific quarter)
- quarterName: String
- average: Number

StudentSchema (Schema that holds the student's name, grades and final grade for the year)
- name: String
- grades: [QuarterGradeSchema]
- finalGrade: Number


## How To's

To calculate the grades, the input text area accepts a String in a specific format.

Scores are preceded by H for homework grades and T by test grades.

Sample Input:

Quarter 1, 2021
Susan Smith H 75 88 94 95 84 68 91 74 100 82 93 T 73 82 81 92 85
John Wright H 86 55 96 78 T 82 89 93 70 74 H 93 85 80 74 76 82 62
Jane Jones T 88 94 100 82 95 H 84 66 74 98 92 85 100 95 96 42 88
Jimmy Doe H 73 99 98 83 85 92 100 60 74 98 92 T 84 96 79 91 95
Suzy Johnson H 65 72 78 80 82 74 76 0 85 75 76 T 74 79 70 83 78
Quarter 2, 2021
Susan Smith H 70 70 70 70 70 70 70 70 70 70 70 T 70 70 70 70 70
John Wright H 70 70 70 70 T 70 70 70 70 70 H 70 70 70 70 70 70 70
Jane Jones T 70 70 70 70 70 H 70 70 70 70 70 70 70 70 70 70 70
Jimmy Doe H 70 70 70 70 70 70 70 70 70 70 70 T 70 70 70 70 70
Suzy Johnson H 70 70 70 70 70 70 70 70 70 70 70 T 70 70 70 70 70
![img.png](img.png)

Based on the input the results are calculated by the following formula:

Quarter Average = (((Total Homework Score - Lowest Homework Score) / Total # of Homework) * .4)
(((Total Test Score) / Total # of Test) * .6)

Final Grade = (Total Quarter Average Score / 4)

![img_1.png](img_1.png)

## Limitations / For Improvements

1. Validation of input is currently not implemented
2. Average scores are only based on the actual input of # of homework or tests and is not based on the expected max number of tests/homeworks
3. Written unit tests

